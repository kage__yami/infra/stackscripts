#!/bin/bash

## USER DEFINED FIELDS
#<UDF name="hostname" label="The hostname to apply to this instance">
# HOSTNAME=
#
#<UDF name="fqdn" label="The FQDN this instance will be accessible via" default="">
# FQDN=
#
#<UDF name="username" label="The name of the administrative user to create on this instance">
# USERNAME=
#
#<UDF name="password" label="The password for the administrative user on this instance">
# PASSWORD=

curl -L https://gitlab.com/isekai/infrastructure/stackscripts/-/raw/main/arch-linux/stackscript.sh > ./stackscript.sh
chmod u+x ./stackscript.sh
./stackscript.sh
